import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Card, Image } from "semantic-ui-react";

import config from "../../config";
import { getAllPhotos } from "../../store/actions/photos";
import Popup from "../../components/UI/Popup/Popup";

class Photo extends Component {
  state = {
    modalOpen: false,
    photo: ''
  };

  componentDidMount() {
    this.props.getAllPhotos();
  }

  handleOpen = (photoUrl) => this.setState({ modalOpen: true, photo: photoUrl});

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    return (
      <div className="container">
        <Card.Group centered>
          {this.props.photos &&
            this.props.photos.map(item => (
              <Fragment key={item._id}>
                <Card onClick={() => this.handleOpen(item.photo)}>
                  <Card.Content>
                    <Image
                      src={config.apiUrl + "/uploads/" + item.photo}
                    />
                    <Card.Header>{item.title}</Card.Header>
                    <Card.Description
                      onClick={() => this.props.history.push("/user/" + item.author._id)} >
                      {"By: " + item.author.displayName}
                    </Card.Description>
                  </Card.Content>
                </Card>
                {this.state.modalOpen ? (
                  <Popup
                    photo={this.state.photo}
                    open={this.state.modalOpen}
                    close={this.handleClose}
                  />
                ) : null}
              </Fragment>
            ))}
        </Card.Group>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    photos: state.photos.photos
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getAllPhotos: () => dispatch(getAllPhotos())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Photo);
