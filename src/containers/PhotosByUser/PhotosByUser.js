import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Card, Header, Icon, Image } from "semantic-ui-react";

import config from "../../config";
import { deletePhoto, getPhotosByUser } from "../../store/actions/photos";
import Popup from "../../components/UI/Popup/Popup";

class PhotosByUser extends Component {
  state = {
    modalOpen: false
  };

  componentDidMount() {
    this.props.getPhotosByUser(this.props.match.params.id);
  }

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    return (
      <div className="container">
        <Header as="h1" dividing>
          {this.props.user && this.props.user.displayName + "'s Gallery"}
        </Header>

        <Card.Group centered>
          {this.props.photos &&
            this.props.photos.map(item => (
              <Fragment key={item._id}>
                <Card>
                  <Card.Content>
                    <Image
                      src={config.apiUrl + "/uploads/" + item.photo}
                      onClick={this.handleOpen}
                    />
                    <Card.Header>{item.title}</Card.Header>
                  </Card.Content>
                  {this.props.user &&
                    this.props.user._id === item.author._id && (
                      <Card.Content extra>
                        <a onClick={() => this.props.deletePhoto(item._id)}><Icon name="delete" />
                          Delete photo
                        </a>
                      </Card.Content>
                    )}
                </Card>
                {this.state.modalOpen ? (
                  <Popup
                    photo={item.photo}
                    open={this.state.modalOpen}
                    close={this.handleClose}
                  />
                ) : null}
              </Fragment>
            ))}
        </Card.Group>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    photos: state.photos.photos,
    user: state.users.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPhotosByUser: id => dispatch(getPhotosByUser(id)),
    deletePhoto: id => dispatch(deletePhoto(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotosByUser);
