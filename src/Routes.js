import React from "react";
import { Route, Switch } from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Photo from "./containers/Photo/Photo";
import PhotosByUser from "./containers/PhotosByUser/PhotosByUser";
import AddNewPhoto from "./components/AddNewPhoto/AddNewPhoto";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Photo} />
      <Route path="/user/:id" exact component={PhotosByUser} />
      <Route path="/add-new-photo" exact component={AddNewPhoto} />
      <Route path="/register" exact component={Register} />
      <Route path="/login" exact component={Login} />
      <Route
        render={() => <h1 style={{ textAlign: "center" }}>Page not found</h1>}
      />
    </Switch>
  );
};

export default Routes;
