import React from "react";
import { Nav, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const UserMenu = ({ user, logout }) => {

  return (
    <Nav pullRight>
      <LinkContainer to={'/user/' + user._id}>
        <NavItem>My photo</NavItem>
      </LinkContainer>
      {user &&
      <LinkContainer to='/add-new-photo'>
        <NavItem>Add new photo</NavItem>
      </LinkContainer>}
      <NavItem divider='true'/>
      <NavItem onClick={logout}>Logout</NavItem>
    </Nav>
  );
};

export default UserMenu;
