import React from "react";
import { LinkContainer } from "react-router-bootstrap";
import { NavItem } from "react-bootstrap";
import Nav from "react-bootstrap/es/Nav";

const AnonymousMenu = () => {
  return (
    <Nav pullRight>
      <LinkContainer to="/register" exact>
        <NavItem>Sign Up</NavItem>
      </LinkContainer>
      <LinkContainer to="/login" exact>
        <NavItem>Login</NavItem>
      </LinkContainer>
    </Nav>
  );
};

export default AnonymousMenu;
