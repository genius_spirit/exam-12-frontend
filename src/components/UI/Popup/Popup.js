import React from "react";
import { Image, Modal } from 'semantic-ui-react'
import config from "../../../config";

const Popup = ({ photo, open, close}) => {
  return (
    <Modal size='fullscreen' open={open} onClose={close} closeIcon >
      <Image centered src={config.apiUrl + '/uploads/' + photo} style={{margin: '0 auto'}}/>
    </Modal>
  );
};

export default Popup;
