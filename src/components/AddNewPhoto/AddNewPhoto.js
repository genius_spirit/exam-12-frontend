import React, { Component } from "react";
import { connect } from "react-redux";
import { Header } from "semantic-ui-react";

import NewPhotoForm from "../NewPhotoForm/NewPhotoForm";
import { addNewPhoto } from "../../store/actions/photos";

class AddNewPhoto extends Component {
  createNewPhoto = data => {
    this.props.addNewPhoto(data);
  };

  render() {
    return (
      <div className="container">
        <Header as="h1" dividing>
          Add new photo
        </Header>
        <NewPhotoForm
          onSubmit={this.createNewPhoto}
          error={this.props.addFormError}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    addFormError: state.photos.addFormError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addNewPhoto: data => dispatch(addNewPhoto(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewPhoto);
