import React, { Component } from "react";
import { Alert, Button, Col, Form, FormGroup } from "react-bootstrap";
import FormElement from "../UI/Form/FormElement/FormElement";

class NewPhotoForm extends Component {
  state = {
    title: "",
    photo: ""
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        {this.props.error && (
          <Alert bsStyle="danger">{this.props.error.message}</Alert>
        )}

        <FormElement
          propertyName="title"
          title="Photo title"
          type="text"
          value={this.state.title}
          changeHandler={this.inputChangeHandler}
        />

        <FormElement
          propertyName="photo"
          title="Photo"
          type="file"
          changeHandler={this.fileChangeHandler}
        />

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">
              Add
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default NewPhotoForm;
