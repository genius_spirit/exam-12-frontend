import { takeEvery, all } from "redux-saga/effects";
import {
  ADD_NEW_PHOTO, DELETE_PHOTO,
  FACEBOOK_LOGIN,
  GET_ALL_PHOTOS,
  GET_PHOTOS_BY_USER,
  LOGIN_USER,
  LOGOUT_USER,
  LOGOUT_USER_EXPIRED,
  REGISTER_USER
} from "../actions/actionTypes";
import {
  facebookLoginSaga,
  loginUserSaga,
  logoutExpiredUserSaga,
  logoutUserSaga,
  registerUserSaga
} from "./users";
import {
  addNewPhotoSaga, deletePhotoSaga,
  getAllPhotosSaga,
  getPhotosByUserSaga
} from "./photos";

export function* watchRegisterUser() {
  yield takeEvery(REGISTER_USER, registerUserSaga);
}

function* facebookLogin() {
  yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga);
}

function* logoutUser() {
  yield takeEvery(LOGOUT_USER, logoutUserSaga);
}

function* loginUser() {
  yield takeEvery(LOGIN_USER, loginUserSaga);
}

function* logoutExpiredUser() {
  yield takeEvery(LOGOUT_USER_EXPIRED, logoutExpiredUserSaga);
}

function* getAllPhotos() {
  yield takeEvery(GET_ALL_PHOTOS, getAllPhotosSaga);
}

function* getPhotosByUser() {
  yield takeEvery(GET_PHOTOS_BY_USER, getPhotosByUserSaga);
}

function* addNewPhoto() {
  yield takeEvery(ADD_NEW_PHOTO, addNewPhotoSaga);
}

function* deletePhoto() {
  yield takeEvery(DELETE_PHOTO, deletePhotoSaga)
}

export function* rootSaga() {
  yield all([
    watchRegisterUser(),
    facebookLogin(),
    logoutUser(),
    loginUser(),
    logoutExpiredUser(),
    getAllPhotos(),
    getPhotosByUser(),
    addNewPhoto(),
    deletePhoto()
  ]);
}
