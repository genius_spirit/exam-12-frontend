import axios from "../../axios-api";
import { put } from "redux-saga/effects";
import { NotificationManager } from "react-notifications";

import {
  addNewPhotoFailure,
  addNewPhotoSuccess, deletePhotoFailure, deletePhotoSuccess,
  getAllPhotosFailure,
  getAllPhotosSuccess,
  getPhotosByUserFailure,
  getPhotosByUserSuccess
} from "../actions/photos";
import { push } from "react-router-redux";

export function* getAllPhotosSaga() {
  try {
    const response = yield axios.get("/photos");
    if (response) yield put(getAllPhotosSuccess(response.data));
  } catch (error) {
    yield put(getAllPhotosFailure(error.response.data));
    NotificationManager.error("Error", "Can't load photos from server");
  }
}

export function* getPhotosByUserSaga(action) {
  try {
    const response = yield axios.get(`/photos/${action.id}`);
    if (response) yield put(getPhotosByUserSuccess(response.data));
  } catch (error) {
    yield put(getPhotosByUserFailure(error.response.data));
  }
}

export function* addNewPhotoSaga(action) {
  try {
    const response = yield axios.post("/photos", action.data);
    yield put(addNewPhotoSuccess(response.data));
    yield put(push("/"));
  } catch (error) {
    yield put(addNewPhotoFailure(error.response.data));
  }
}

export function* deletePhotoSaga(action) {
  try {
    const response = yield axios.delete(`/photos/${action.id}`);
    yield put(deletePhotoSuccess(response.data));
    yield put(push('/'));
  } catch (error) {
    yield put(deletePhotoFailure(error.response.data))
  }
}
