import {
  ADD_NEW_PHOTO_FAILURE, DELETE_PHOTO_FAILURE,
  GET_ALL_PHOTOS_FAILURE,
  GET_ALL_PHOTOS_SUCCESS,
  GET_PHOTOS_BY_USER_FAILURE,
  GET_PHOTOS_BY_USER_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  photos: [],
  error: null,
  addFormError: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_PHOTOS_SUCCESS:
      return {...state, photos: action.photo};
    case GET_ALL_PHOTOS_FAILURE:
      return {...state, error: action.error};
    case GET_PHOTOS_BY_USER_SUCCESS:
      return {...state, photos: action.photo};
    case GET_PHOTOS_BY_USER_FAILURE:
      return {...state, error: action.error};
    case ADD_NEW_PHOTO_FAILURE:
      return {...state, addFormError: action.error};
    case DELETE_PHOTO_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;