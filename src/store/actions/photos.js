import {
  ADD_NEW_PHOTO,
  ADD_NEW_PHOTO_FAILURE,
  ADD_NEW_PHOTO_SUCCESS, DELETE_PHOTO, DELETE_PHOTO_FAILURE, DELETE_PHOTO_SUCCESS,
  GET_ALL_PHOTOS,
  GET_ALL_PHOTOS_FAILURE,
  GET_ALL_PHOTOS_SUCCESS,
  GET_PHOTOS_BY_USER,
  GET_PHOTOS_BY_USER_FAILURE,
  GET_PHOTOS_BY_USER_SUCCESS
} from "./actionTypes";

export const getAllPhotos = () => {
  return { type: GET_ALL_PHOTOS };
};

export const getAllPhotosSuccess = photo => {
  return { type: GET_ALL_PHOTOS_SUCCESS, photo };
};

export const getAllPhotosFailure = error => {
  return { type: GET_ALL_PHOTOS_FAILURE, error };
};

export const getPhotosByUser = id => {
  return { type: GET_PHOTOS_BY_USER, id };
};

export const getPhotosByUserSuccess = photo => {
  return { type: GET_PHOTOS_BY_USER_SUCCESS, photo };
};

export const getPhotosByUserFailure = error => {
  return { type: GET_PHOTOS_BY_USER_FAILURE, error };
};

export const addNewPhoto = data => {
  return { type: ADD_NEW_PHOTO, data };
};

export const addNewPhotoSuccess = data => {
  return { type: ADD_NEW_PHOTO_SUCCESS, data };
};

export const addNewPhotoFailure = error => {
  return { type: ADD_NEW_PHOTO_FAILURE, error };
};

export const deletePhoto = (id) => {
  return {type: DELETE_PHOTO, id};
};

export const deletePhotoSuccess = () => {
  return { type: DELETE_PHOTO_SUCCESS };
};

export const deletePhotoFailure = error => {
  return {type: DELETE_PHOTO_FAILURE, error};
};
